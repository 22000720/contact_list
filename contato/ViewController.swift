//
//  ViewController.swift
//  contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    struct Contact{
        let name : String
        let number : String
        let address : String
        let email : String
    }
    var contactList : [Contact] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! Cell
        let contact = contactList[indexPath.row]
        cell.name.text = contact.name
        cell.address.text = contact.address
        cell.email.text = contact.email
        cell.number.text = contact.name
        return cell
    }
    

    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        TableView.dataSource = self;
        contactList.append(Contact(name : "Jose", number: "31 95670-2141", address: "Wall street 1", email: "jose@jose.com"))
        contactList.append(Contact(name : "Maria", number: "31 98901-3456", address: "Wall street 2", email: "maria@maria.com"))
        contactList.append(Contact(name : "James", number: "31 24613-4246", address: "Wall street 3", email: "james@james.com"))
        contactList.append(Contact(name : "William", number: "31 12581-4691", address: "Wall street 4", email: "william@william.com"))
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

}

